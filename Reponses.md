# 1. Développement REST : récupération des sources

## d. testez le manuellement avec la commande curl

Créer deux ingrédients
```
curl -i --noproxy localhost --data-raw '{"name":"chorizo"}' -H 'Content-Type: application/json' -v localhost:8080/api/v1/ingredients
```
```
curl -i --noproxy localhost --data-raw '{"name":"pâte"}' -H 'Content-Type: application/json' -v localhost:8080/api/v1/ingredients
```
Récupérer la liste des ingrédients
```
curl --noproxy localhost localhost:8080
```

plus tard