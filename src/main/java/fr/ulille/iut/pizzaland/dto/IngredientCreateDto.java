package fr.ulille.iut.pizzaland.dto;

import java.net.URI;

import javax.ws.rs.POST;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class IngredientCreateDto {
	
	
	private String name;
	
	public IngredientCreateDto() {}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	public static IngredientCreateDto toCreateDto(Ingredient ingredient) {
	    IngredientCreateDto dto = new IngredientCreateDto();
	    dto.setName(ingredient.getName());
	    
	    return dto;
	}

	public static Ingredient fromIngredientCreateDto(IngredientCreateDto dto) {
	    Ingredient ingredient = new Ingredient();
	    ingredient.setName(dto.getName());

	    return ingredient;
	}

}
